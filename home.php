<?php
	include_once 'dbconfig.php';
	if(!$user->isLoggedIn())
	{
 		$user->redirect('index.php');
	}
	$user_id = $_SESSION['user_id'];
	$stmt = $gDbConn->prepare("SELECT * FROM users WHERE id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));
	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head>
  <title>Tuto HighCharts.js</title>
	 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-		0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="./css/home.css">

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/highcharts.js"></script>
<script>

	var chart;	//<< variable du graphe globale car utilisée dans 2 fonctions

	/**
	 * Request data from the server, add it to the graph and set a timeout 
	 * to request again
	 */
	function requestData() {
		$.ajax({
		url: 'live_server_data.php',
		
		success: function(point) {
			var series = chart.series[0];
			var shift = series.data.length > 5; // shift if the series is 
			// longer than 5
			var lastIdx = series.data.length > 0 ? series.data.length - 1 : null;

			if (lastIdx != null) {
				console.log("series.data[" + lastIdx + "] : " + series.data[lastIdx].x );
				console.log("point[0] : " + point[0] );
			}

			// add the point
			if( lastIdx == null  || (series.data[ lastIdx ].x !== point[0]) ) {
				chart.series[0].addPoint(point, false, shift);
				chart.redraw();
			}

			// call it again after one second
			setTimeout(requestData, 10000);    
		},
	
		cache: false,

		complete: function(resultat, statut) {
			//alert("Ajax Complete : " + resultat + " / " + statut);
		},

		error: function(exception){alert('Exception:' + exception);}
	});
	}

	$(document).ready(function() {

		// Ne pas utiliser l'heure UTC au profit de l'heure locale
		Highcharts.setOptions({
			global: {
				useUTC: false
			}
		});

		chart = new Highcharts.Chart({
		chart: {
		renderTo: 'container',
				type: 'spline',
				events: {
                load: requestData
            }
			},
			title: {
				text: 'Supervision de traffic réseau'
			},
			xAxis: {
				//categories : [],
				labels: {
					formatter: function() {
						return Highcharts.dateFormat('%d-%m-%Y<br/>%H:%M:%S', this.value)
					},
					rotation: 45,
					step: 2,
					align: 'left',
					y: 20
				}
			},
			yAxis: {
				title: {
					text: 'Nombre d\'octets'
				}
			},
			tooltip: {
				formatter: function () {
					return 'Variation du traffic entrant au ' + Highcharts.dateFormat('<b>%d-%m-%Y</b><br/> à <b>%H:%M:%S</b>', this.x) + ' : +' + this.y + ' octets.';
				}
			},
			series: [{
				name: 'Traffic entrant',
				data: []
			}]
		});

	});



	</script>

 </head>
 <body>


<nav class="navbar navbar-default"> 
<div class=container-fluid> 
	<div class=navbar-header> 
		<a class=navbar-brand href=#><img alt="Brand" src="./img/dashboard.svg" height=30></a>	
	</div> 
		<button type=button class="btn btn-default navbar-btn navbar-right"><a href="logout.php?logout">Déconnexion</a></button> 
</div> 
</nav> 
		
<h1 id="welcome">Bienvenue <?php print($userRow['name']); ?></h1>

	<div id="container">
	</div>
</div>
</body>
</html>



