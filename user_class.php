<?php
class User
{
	private $db;

	function __construct($theDbConnexion)
	{
		$this->db = $theDbConnexion;
	}

	public function register($theName,$theMail,$thePassword)
	{
		try
		{
			$password = password_hash($thePassword, PASSWORD_DEFAULT);

			$stmt = $this->db->prepare("INSERT INTO users(name,email,password) VALUES(:nphName, :nphMail, :nphPassword)");

			$stmt->bindparam(":nphName", $theName);
			$stmt->bindparam(":nphMail", $theMail);
			$stmt->bindparam(":nphPassword", $password);            
			$stmt->execute(); 

			return $stmt; 
		}
		catch(PDOException $e)
		{
			die('Erreur : ' . $e->getMessage());
		}    
	}

	public function login($theName,$thePassword)
	{
		try
		{
			/* Création requête préparée avec paramètre nommé ( ex. :nphName avec nph signifiant "named place holder") */
			$stmt = $this->db->prepare('SELECT * FROM users WHERE name=:nphName LIMIT 1;');
			$stmt->bindparam(':nphName',$theName);
			$stmt->execute();
			//$stmt->debugDumpParams();
			$userRow = $stmt->fetch(PDO::FETCH_ASSOC);

			//if($stmt->rowCount() > 0)
			//{
			// -> rowCount() ne fonctionne pas aves SQLite. A remplacer par ce qui suit :
			if($userRow && count($userRow))
			{
				// contrôler le mot de passe crypté avec la fonction
				// paswword_hash() en utilisant l'algorithme par défaut
				// (apparemment Blowfish en 2016)
				if(password_verify($thePassword, $userRow['password']))
				{
					$_SESSION['user_id'] = $userRow['id'];
					return true;
				}
				else
				{
					return false;
				}
			} else {
				//echo 'pas de résultat à la requete';
			}
		}
		catch(PDOException $e)
		{
			die('Erreur : ' . $e->getMessage());
		}
	}

	public function isloggedIn()
	{
		if(isset($_SESSION['user_id']))
		{
			return true;
		}
	}

	public function redirect($url)
	{
		header("Location: $url");
	}

	public function logout()
	{
		session_destroy();
		unset($_SESSION['user_id']);
		return true;
	}
}
?>
