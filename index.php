<?php
// http://codepen.io/marcobiedermann/pen/dsbFy
// http://www.codingcage.com/2015/04/php-login-and-registration-script-with.html
// http://www.allaboutphp.com/php-login-and-registration-script-with-pdo-and-oop/
require_once 'dbconfig.php';

$error = false;

if($user->isLoggedIn())
{
	$user->redirect('home.php');
}

if(isset($_POST['btn_login']))
{
	$uName = $_POST['frm_name'];
	$uPassword = $_POST['frm_password'];

	//echo $uName. '/'. $uPassword;
	if($user->login($uName,$uPassword))
	{
		$user->redirect('home.php');
	}
	else
	{
		$error = true;
	} 
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Tuto HighCharts</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-		0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<script type="text/javascript" src="/js/jquery.min.js"></script> 

		<link rel="stylesheet" href="./css/signin.css">
	</head>
	<body>
<!--
		<div class="container">

			<div id="login-form">

				<h3>Authentification</h3>

				<form action="#" method="post">
					<fieldset>

						<input type="text" required name="frm_name" placeholder="<nom utilisateur>">

						<input type="password" required name="frm_password" placeholder="<mot de passe>"> 

					<span><?php echo $error; ?></span>

						<input type="submit" name="btn_login" value="Se connecter">

					</fieldset>

				</form>
			</div>

		</div>-->

<?php if ($error) echo "<script type=\"text/javascript\">
$(document).ready(function() {
	$(\"#error\").show();
}
)"
?>
</script>

 <div class="container">

      <form class="form-signin" action="#" method="post">
        <h2 class="form-signin-heading"><img alt="Brand" src="./img/dashboard.svg" width=300px></h2>
        <label for="inputEmail" class="sr-only">Nom utilisateur</label>
        <input type="text" id="inputText" class="form-control" name="frm_name" placeholder="Nom utilisateur" required autofocus>
        <label for="inputPassword" class="sr-only">Mot de passe</label>
		  <input type="password" id="inputPassword" class="form-control" name="frm_password"  placeholder="Mot de passe" required>
				  <button class="btn btn-lg btn-primary btn-block" name="btn_login" type="submit">Se connecter</button>
	<div id="error">

				Echec d'identification<br />
				(Essayer 'root/admin' ou 'john/123456')
			</div>

      </form>

    </div> <!-- /container -->
	</body>
</html>

