<?php
include_once 'dbconfig.php';

if(!$user->isLoggedIn())
{
	$user->redirect("index.php");
}
else 
{
	$user->redirect("home.php");
}

if(isset($_GET['logout']))
{
	$user->logout();
	$user->redirect("index.php");
}

?>
