<?php

session_start();

$gDbDsn = "iftrafficdb.sqlite";

try
{
     $gDbConn = new PDO("sqlite:{$gDbDsn}");
     $gDbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
     echo $e->getMessage();
}

include_once 'user_class.php';
$user = new User($gDbConn);

