<?php
// Set the JSON header
header("Content-type: text/json");

try {
	// On se connecte à la BDD
	$file_db = new PDO('sqlite:iftrafficdb.sqlite');

	// Set errormode to exceptions
	$file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	// On lit le dernier enregistrements de la BDD
	$dataSet1 = $file_db->query('SELECT * FROM ifstats ORDER BY id DESC LIMIT 1;');
	// On lit l'avant dernier enregistrement
	$dataSet2 = $file_db->query('SELECT * FROM ifstats ORDER BY id DESC LIMIT 1 OFFSET 1;'); //LIMIT 1 OFFSET (SELECT COUNT(*) FROM ifstats)-2;');

	// On ferme la connexion à la BDD
	$file_db = null;
}
catch (PDOException $e)
{
	die('Erreur : ' . $e->getMessage());
}

if($dataSet1) {
	$endRow = $dataSet1->fetch();
	//print_r($endRow);
	if(! $dataSet2) {
		$delta = $endRow['rx_bytes'];
	} else {
		$lastRow = $dataSet2->fetch();
		//print_r($lastRow);
		$delta = $endRow['rx_bytes'] - $lastRow['rx_bytes'];
		if ($delta < 0) {
			$delta = $endRow['rx_bytes'];
		}
	}
	// Créer un tableau PHP et l'envoyer en JSON
	$t = 0 + $endRow['timestamp'];
	$ret = array($t, $delta);
} else {
	$ret = array(-1,-1);
}
echo json_encode($ret);
?>
