#! /bin/bash

# Variable qui définit la racine du site web
WEBROOT=~claude/public_html/labdashboard

# Acquisition des valeurs de traffic réseau
# Remarque : l'opérateur de redirection d'entrée '<<<' n'est reconnu que dans l'interpréteur bash
# (-> nécessité de préciser l'utilisation de bash dans le shebang)
# * Linux :
#read rxb rxp txb txp <<< "$(cat /sys/class/net/eth0/statistics/?x_[bp]* | tr '\n' ' ')"
# * OSX :
read rxp rxb txp txb <<< "$(/usr/sbin/netstat -ib | grep `hostname` | tr -s ' ' | cut -d ' ' -f 5,7,8,10)"

# Lecture de l'heure
# Remarque : ne pas mettre d'espaces de part et d'autre du signe '='
#timestamp="$(date --iso-8601=seconds)"
timestamp="$(date +%s)"
timestamp=$(expr $timestamp \* 1000)

# Mise à jour de la BDD
sqlite3 $WEBROOT/iftrafficdb.sqlite "insert into ifstats (timestamp,rx_bytes,rx_packets, tx_bytes, tx_packets) values ('$timestamp',$rxb, $rxp, $txb, $txp);"
